__author__ = 'Ry10'

import subprocess as sp
import os
import shutil
import sys
import string
import time
import numpy as np
from scipy.interpolate import RectBivariateSpline, bisplev

xfoilpath = r'C:\Users\Ry10\Documents\GitHub\XFOIL6.99\xfoil.exe'


def Xfoil(name, Re, family):#Ncrit, Re ):
    def Cmd(cmd):
        ps.stdin.write(cmd+'\n')
    try:
        os.remove(name+'.log')
    except :
        pass
    #    print ("no such file")
    # run xfoil
    ps = sp.Popen(xfoilpath ,stdin=sp.PIPE,stderr=sp.PIPE,stdout=sp.PIPE)
    ps.stderr.close()
    # comand part
    Cmd('naca ' + family + name)#'load '+name+'.dat')
    Cmd('OPER')
    #Cmd('Vpar')
    #Cmd('N '+str(Ncrit))
    #Cmd(' ')
    Cmd('visc '+ str(Re))
    Cmd('alfa 0')
    Cmd('PACC')
    Cmd(family + name + str(Re) + '.log')  # output file
    Cmd(' ')          # no dump file
    Cmd('aseq -20.0 20.0 0.5')
    Cmd('PACC')
    Cmd(' ')     # escape OPER
    Cmd('quit')  # exit
    #resp = ps.stdout.read()
    #print "resp:",resp   # console ouput for debug
    ps.stdout.close()
    ps.stdin.close()
    ps.wait()
    #while (ps.returncode() == None):
    #    time.sleep(1)
    #ps.kill()

def parseXFOIL(name, Re, family):
    filename = family + name + str(Re) + '.log'
    f = open(filename, 'r')
    flines = f.readlines()
    alpha = 0
    C_L = 0
    C_D = 0
    LDmax = 0
    points = np.append([[0, 0, 0],[0,0,0]], [[0, 0, 0]], axis=0)
    point = np.zeros(3)
    for i in range(12,len(flines)):
        #print flines[i]
        words = string.split(flines[i])
        LD = float(words[1])/float(words[2])
        alpha = float(words[0])
        C_L = float(words[1])
        C_D = float(words[2])
        point[0] = alpha
        point[1] = C_L
        point[2] = C_D
        points = np.append(points, [point], axis=0)
        if(LD>LDmax):
            LDmax = LD
    return points

def spline(t, alpha, C_D, C_L):
        alpha = np.radians(alpha)

        # special case if zero or one Reynolds number (need at least two for bivariate spline)
        # if len(Re) < 2:
        #     Re = [1e1, 1e15]
        #     cl = np.c_[cl, cl]
        #     cd = np.c_[cd, cd]
        #     self.one_Re = True

        kx = min(len(alpha)-1, 3)
        ky = min(len(t)-1, 3)
        C_D2 = np.zeros((len(alpha), len(t)))
        C_L2 = np.zeros((len(alpha), len(t)))
        print alpha.size
        print t.size
        print C_D.size
        for i in range(0, len(alpha)):
            for j in range(0, len(t)-2):
                C_D2[i][j] = C_D[i][j]
                C_L2[i][j] = C_L[i][j]
        # a small amount of smoothing is used to prevent spurious multiple solutions
        cl_spline = RectBivariateSpline(alpha, t, C_L2, kx=kx, ky=ky, s=0.1)
        cd_spline = RectBivariateSpline(alpha, t, C_D2, kx=kx, ky=ky, s=0.001)
        return cd_spline, cl_spline

