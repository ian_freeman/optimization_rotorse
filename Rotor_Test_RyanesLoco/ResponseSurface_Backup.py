__author__ = 'Ry10'

# import statements

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import interpolate
from scipy import arange, array#, exp
from airfoilprep import Polar, Airfoil
import subprocess as sp
import os
import shutil
import sys
import string
import time
import numpy as np
from scipy.interpolate import RectBivariateSpline, bisplev, interp1d
# from xfoil import *
import math as math
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


class ResponseSurface(object):

    def __init__(self, Re, thick_min, thick_max):
        """Constructor

        Parameters
        ----------
        Re : float
            Reynolds number
        thick_max : int
            Maximum thickness for XFOIL
        thick_min : int
            Minimum thickness for XFOIL
        """

        self.Re = Re

        self.thick_max = thick_max
        self.thick_min = thick_min
        self.thick_len = thick_max - thick_min + 1
        extrapolated_length = 165
        self.alpha_min = -20
        self.alpha_max = 20
        self.alpha_increment = 0.5
        self.alphas = np.linspace(self.alpha_min, self.alpha_max, (self.alpha_max - self.alpha_min)/self.alpha_increment + 1)
        self.alpha_len = len(self.alphas)
        self.alpha = np.zeros(self.alpha_len*self.thick_len)

        self.total_length = self.alpha_len*self.thick_len
        self.total_length_all = self.thick_len*extrapolated_length

        self.thick_tot = np.zeros(self.total_length_all)
        self.thicknesses = np.linspace(self.thick_min, self.thick_max, (self.thick_max - self.thick_min) + 1)
        self.alpha_total = np.zeros(self.total_length_all)
        self.alpha_extrapolated = np.zeros(extrapolated_length)
        self.points = np.zeros((self.total_length_all, 3)) #append([[0, 0, 0], [0, 0, 0]], [[0, 0, 0]], axis=0)
        # self.points = np.zeros((10000, 3))

        self.C_D_column = np.zeros(self.total_length)
        self.C_D_grid = np.zeros((extrapolated_length, self.thick_len)) #(self.alpha_len+1000, self.thick_len))
        self.C_D_extrapol_tot = np.zeros(self.total_length_all)

        self.C_L_column = np.zeros(self.total_length)
        self.C_L_grid = np.zeros((extrapolated_length, self.thick_len)) #(self.alpha_len+1000, self.thick_len))
        self.C_L_extrapol_tot = np.zeros(self.total_length_all)

        self.C_M_column = np.zeros(self.total_length)
        self.C_M_grid = np.zeros((extrapolated_length, self.thick_len)) #(self.alpha_len+1000, self.thick_len))
        self.C_M_extrapol_tot = np.zeros(self.total_length_all)

        self.t = np.zeros(self.alpha_len*self.thick_len)
        self.increment_thickness = 0
        self.increment_total = 0

        self.X = np.zeros(0)
        self.Y = np.zeros(0)
        self.n = 200
        self.F = np.zeros((self.n, self.n))
        self.F2 = np.zeros((self.n, self.n))
        self.cl_store = np.zeros(extrapolated_length)
        self.cd_store = np.zeros(extrapolated_length)
        self.cm_store = np.zeros(extrapolated_length)
        self.cl_change = np.zeros(extrapolated_length)
        self.cd_change = np.zeros(extrapolated_length)
        self.cm_change = np.zeros(extrapolated_length)
        
    def XFOIL(self, name, Re, family):
        # xfoilpath = r'C:\Users\Ry10\Documents\GitHub\XFOIL6.99\xfoil.exe'
        xfoilpath = r'/Applications/Xfoil.app/Contents/MacOS/Xfoil'
        xfoilpath = r'/Applications/Xfoil.app/Contents/Resources/xfoil'
        def Cmd(cmd):
            ps.stdin.write(cmd+'\n')
        try:
            os.remove(name+'.log')
        except :
            pass
        #    print ("no such file")
        # run xfoil
        ps = sp.Popen(xfoilpath, stdin=sp.PIPE, stderr=sp.PIPE, stdout=sp.PIPE)
        ps.stderr.close()
        # comand part
        Cmd('naca ' + family + name)#'load '+name+'.dat')
        Cmd('OPER')
        #Cmd('Vpar')
        #Cmd('N '+str(Ncrit))
        #Cmd(' ')
        Cmd('visc '+ str(Re))
        Cmd('alfa 0')
        Cmd('PACC')
        Cmd(family + name + str(Re) + '.log')  # output file
        Cmd(' ')          # no dump file
        Cmd('aseq -20.0 20.0 0.5')
        Cmd('PACC')
        Cmd(' ')     # escape OPER
        Cmd('quit')  # exit
        #resp = ps.stdout.read()
        #print "resp:",resp   # console ouput for debug
        ps.stdout.close()
        ps.stdin.close()
        ps.wait()
        #while (ps.returncode() == None):
        #    time.sleep(1)
        #ps.kill()    

    def parseXFOIL(self, name, Re, family):
        filename = family + name + str(Re) + '.log'
        f = open(filename, 'r')
        flines = f.readlines()
        points = np.zeros((self.alpha_len, 3))
        C_L_raw = np.zeros(0)
        C_D_raw = np.zeros(0)
        C_M_raw = np.zeros(0)
        alphas_raw = np.zeros(0)
        # points = np.append([[0, 0, 0], [0, 0, 0]], [[0, 0, 0]], axis=0)
        # point = np.zeros(3)
        for i in range(12 - 12, len(flines) - 12):
            words = string.split(flines[i+12])
            alphas_raw = np.append(alphas_raw, float(words[0]))
            C_L_raw = np.append(C_L_raw, float(words[1]))
            C_D_raw = np.append(C_D_raw, float(words[2]))
            C_M_raw = np.append(C_M_raw, float(words[4]))
        # if len(C_L_raw) == 0 or name == '50' or name == '49':
        #     alphas_raw = self.alphas
        #     C_L_raw = np.linspace(0, 0, len(self.alphas))
        #     C_D_raw = np.linspace(0, 0, len(self.alphas))
        cl_inter = interp1d(alphas_raw, C_L_raw, kind='cubic')
        cd_inter = interp1d(alphas_raw, C_D_raw, kind='cubic')
        cm_inter = interp1d(alphas_raw, C_M_raw, kind='cubic')
        cl_extra = self.extrap1d(cl_inter)
        cd_extra = self.extrap1d(cd_inter)
        cm_extra = self.extrap1d(cm_inter)
        cl_total = cl_extra(self.alphas)
        cd_total = cd_extra(self.alphas)
        cm_total = cm_extra(self.alphas)
        # plt.figure()
        # plt.plot(alphas_raw, C_L_raw, 'o', self.alphas, cl_total, 'x-')
        # plt.figure()
        # plt.plot(alphas_raw, C_D_raw, 'o', self.alphas, cd_total, 'x-')
        # plt.show()
        return cl_total, cd_total, cm_total
    
    def raw_XFOIL(self, xfoil):
        family = '23'
        file = open("newfile.txt", "w")
        for i in range(self.thick_min, self.thick_max, 1):
            if i < 10:
                name = '0' + str(i)
            else:
                name = str(i)
            reynolds1 = 3e6
            reynolds2 = 5e6
            if xfoil: 
                self.XFOIL(name, reynolds1, family)
                #self.XFOIL(name, reynolds2, family)
            cl_total, cd_total, cm_total = self.parseXFOIL(name, reynolds1, family)
            #cl_total_2, cd_total_2 = self.parseXFOIL(name, reynolds2, family)
            p1 = Polar(reynolds1, self.alphas, cl_total, cd_total)
            #p2 = Polar(reynolds1, self.alphas, cl_total_2, cd_total_2)
            #af = Airfoil([p1, p2])
            af = Airfoil([p1])
            r_over_R = 0.5
            chord_over_r = 0.15
            tsr = 5.0
            af3D = af.correction3D(r_over_R, chord_over_r, tsr)
            af_extrap1 = af3D.extrapolate(1.5)
            af_extrap1.writeToAerodynFile(name+'extrap')
            alpha_ext, Re_ext, cl_ext, cd_ext = af_extrap1.createDataGrid()
            if i > 46 and i < 51:
                if i == 47:
                    self.cl_store = cl_ext
                    self.cd_store = cd_ext
                if i == 48:
                    self.cl_change = cl_ext - self.cl_store
                    self.cd_change = cd_ext - self.cd_store
                if i == 49:
                    cl_ext = self.cl_store #+ 2 * self.cl_change
                    cd_ext = self.cd_store #+ 2 * self.cd_change
                if i == 50:
                    cl_ext = self.cl_store #+ 3 * self.cl_change
                    cd_ext = self.cd_store #+ 3 * self.cd_change
            self.alpha_extrapolated = alpha_ext
            # if i == self.thick_min:
            #     plt.figure()
            #     plt.plot(alpha_ext, cl_ext[:, 0], '-xr')
            #     plt.xlabel('alpha')
            #     plt.ylabel('C_L')
            #     plt.title('C_L for NACA 2323')
            #     plt.figure()
            #     plt.plot(alpha_ext, cd_ext[:, 0], '-xb')
            #     plt.xlabel('alpha')
            #     plt.ylabel('C_D')
            #     plt.title('C_D for NACA 2323')
            #     plt.show()
            for k in range(0, len(alpha_ext), 1):
                self.alpha_total[k + self.increment_total] = alpha_ext[k]
                self.C_D_extrapol_tot[k + self.increment_total] = cd_ext[k, 0]
                self.C_L_extrapol_tot[k + self.increment_total] = cl_ext[k, 0]
                self.thick_tot[k + self.increment_total] = i
                self.increment_total += 1
            # points = np.append(self.points, polar1, axis=0)
            # size = polar1.shape
            for j in range(0, len(self.alpha_extrapolated), 1):
                # Add the 2D arrays
                if math.isnan(cl_ext[j][0]):
                    self.C_L_grid[j][i-self.thick_min] = 0
                else:
                    self.C_L_grid[j][i-self.thick_min] = cl_ext[j][0]
                if math.isnan(cd_ext[j][0]):
                    self.C_D_grid[j][i-self.thick_min] = 0
                else:
                    self.C_D_grid[j][i-self.thick_min] = cd_ext[j][0]
            self.increment_thickness += len(self.alphas)
        file.close()

    def extrap1d(self, interpolator):
        xs = interpolator.x
        ys = interpolator.y

        def pointwise(x):
            if x < xs[0]:
                return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
            elif x > xs[-1]:
                return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
            else:
                return interpolator(x)

        def ufunclike(xs):
            return array(map(pointwise, array(xs)))

        return ufunclike

    def extrapolate(self, xfoil_on):
        self.raw_XFOIL(xfoil_on)
        # kx = min(len(alpha)-1, 3)
        # ky = min(len(thick)-1, 3)
        cl_spline = RectBivariateSpline(self.alpha_extrapolated, self.thicknesses, self.C_L_grid) #, kx=kx, ky=ky, s=0.1)
        cd_spline = RectBivariateSpline(self.alpha_extrapolated, self.thicknesses, self.C_D_grid) #, kx=kx, ky=ky, s=0.001)
        CD_Spline = np.zeros((len(self.thicknesses), len(self.alpha_extrapolated)))
        for i in range(0, len(self.thicknesses)):
            for j in range(0, len(self.alphas)):
                CD_Spline[i][j] = cd_spline.ev(self.alpha_extrapolated[i], self.thicknesses[i])
        n = 200
        thick = np.linspace(self.thick_min, self.thick_max, n)
        alpha = np.linspace(-180, 180, n)
        [self.X, self.Y] = np.meshgrid(alpha, thick)
        for i in range(n):
            for j in range(n):
                self.F[i, j] = cd_spline.ev(self.X[i, j], self.Y[i, j])
                self.F2[i, j] = cl_spline.ev(self.X[i, j], self.Y[i, j])
        return cd_spline, cl_spline


    def createAeroFile(self, filename, Re, cl_spline, cd_spline, t):
        alphas = np.linspace(-180, 180, 73)
        cl = cl_spline(alphas, t)
        cd = cd_spline(alphas, t)
        f = open(filename, 'w')
        print >> f, 'AeroDyn airfoil file.'
        print >> f, 'Compatible with AeroDyn v13.0.'
        print >> f, 'Generated by airfoilprep.py'
        print >> f, '{0:<10d}\t\t{1:40}'.format(1, 'Number of airfoil tables in this file')
        print >> f, '{0:<10f}\t{1:40}'.format(Re/1e6, 'Reynolds number in millions.')
        print >> f, '{0:<10f}\t{1:40}'.format(0.0, 'Control setting')
        print >> f, '{0:<10f}\t{1:40}'.format(9.00, 'Stall angle (deg)')
        print >> f, '{0:<10f}\t{1:40}'.format(-1.3430, 'Angle of attack for zero Cn for linear Cn curve (deg)')
        print >> f, '{0:<10f}\t{1:40}'.format(7.4888, 'Cn slope for zero lift for linear Cn curve (1/rad)')
        print >> f, '{0:<10f}\t{1:40}'.format(1.3519, 'Cn at stall value for positive angle of attack for linear Cn curve')
        print >> f, '{0:<10f}\t{1:40}'.format(-0.3226, 'Cn at stall value for negative angle of attack for linear Cn curve')
        print >> f, '{0:<10f}\t{1:40}'.format(0.00, 'Angle of attack for minimum CD (deg)')
        print cd_spline(0, t)[0][0]
        print >> f, '{0:<10f}\t{1:40}'.format(cd_spline(0, t)[0][0], 'Minimum CD value')
        for i in range(len(alphas)):
            print >> f, '{:<10f}\t{:<10f}\t{:<10f}'.format(alphas[i], cl[i][0], cd[i][0])
        print >> f, 'EOT'
        f.close()
                
    def plotting(self):
        # fig = plt.figure()
        # ax_cd = fig.add_subplot(111, projection='3d')
        # ax_cd.scatter(self.thick_tot, self.alpha_total, self.C_D_extrapol_tot) #, c='g', marker='o')
        # ax_cd.set_xlabel('t/c')
        # ax_cd.set_ylabel('alpha')
        # ax_cd.set_zlabel('C_D')
        # ax_cd.set_title('NACA 23XX XFOIL C_D Graph - Airfoilprep')
        # plt.xlim(self.thick_min, self.thick_max)
        #
        # fig2 = plt.figure()
        # ax_cl = fig2.add_subplot(111, projection='3d')
        # ax_cl.scatter(self.thick_tot, self.alpha_total, self.C_L_extrapol_tot, c='g')
        # ax_cl.set_xlabel('t/c')
        # ax_cl.set_ylabel('alpha')
        # ax_cl.set_zlabel('C_L')
        # ax_cl.set_title('NACA 23XX XFOIL C_L Graph - Airfoilprep')
        # plt.xlim(self.thick_min, self.thick_max)
        #
        # plt.figure()
        # plt.contourf(self.X, self.Y, self.F, 100)
        # plt.xlabel('alpha')
        # plt.ylabel('thickness')
        # plt.title('C_D')
        #
        # plt.figure()
        # plt.contourf(self.X, self.Y, self.F2, 100)
        # plt.xlabel('alpha')
        # plt.ylabel('thickness')
        # plt.title('C_L')

        fig4 = plt.figure()
        ax4 = fig4.gca(projection='3d')
        surf = ax4.plot_surface(self.X, self.Y, self.F, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
        plt.title('C_D Surface')
        plt.xlabel('alpha')
        plt.ylabel('thickness')
        fig4.colorbar(surf) #, shrink=0.5, aspect=5)

        fig5 = plt.figure()
        ax5 = fig5.gca(projection='3d')
        surf2 = ax5.plot_surface(self.X, self.Y, self.F2, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
        fig5.colorbar(surf2) #, shrink=0.5, aspect=5)
        plt.title('C_L Surface')
        plt.xlabel('alpha')
        plt.ylabel('thickness')
        # ax4.zaxis.set_major_locator(LinearLocator(10))
        # ax4.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        print "Plotting finished"