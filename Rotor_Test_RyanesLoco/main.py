__author__ = 'ian'

import numpy as np
import rotoraerodefaults
import os
import matplotlib.pyplot as plt
import scipy.optimize as opt
import openmdao
import akima
from rotor import RotorSE
from precomp import Orthotropic2DMaterial
from precomp import CompositeSection
from precomp import Profile
from openmdao.main.datatypes.api import Float

from pyOpt import Optimization, ALPSO, NSGA2, SLSQP

from ResponseSurface import ResponseSurface
from copy import deepcopy
from math import pi
from ccblade import CCAirfoil

# global vars for aeroStructural optimization
rotor = None
capTriaxThk = None
capCarbThk = None
tePanelTriaxThk = None
tePanelFoamThk = None

eta_strain = 1.35*1.3*1.0
eta_dfl = 1.35*1.1*1.0
# strain_ult_spar = 1.0e-2
# strain_ult_te = 2500*1e-6
strain_ult_spar = 1.0e-2
strain_ult_te = 2500*1e-6

# material and composite layup data
gBasepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), '5MW_PreCompFiles')

#instantiate variable-speed variable-pitch rotor
# cdf_type = 'weibull'
# rotor = rotoraerodefaults.RotorAeroVSVPWithCCBlade(cdf_type)

# Initialize thickness starting point
length = 35
x0 = np.zeros(length)
x0[0] = 40.0
x0[1] = 35.0
x0[2] = 30.0
x0[3] = 25.0
x0[4] = 21.0
x0[5] = 17.0
x0[6] = 7.55  # tip speed ratio
x0[7] = 3.2612
x0[8] = 4.5709
x0[9] = 3.3178
x0[10] = 1.4621
x0[11] = 13.2783
x0[12] = 7.46036
x0[13] = 2.89317
x0[14] = -0.0878099
#Cap Triax
x0[15] = 3.0e-03
x0[16] = 2.9e-03
x0[17] = 2.8e-03
x0[18] = 2.75e-03
x0[19] = 2.7e-03
#Cap Carbon
x0[20] = 4.2e-02
x0[21] = 2.5e-02
x0[22] = 1.0e-02
x0[23] = 9.0e-03
x0[24] = 6.580e-03
#TE Panel Triax
x0[25] = 3.0e-03
x0[26] = 2.9e-03
x0[27] = 2.8e-03
x0[28] = 2.75e-03
x0[29] = 2.7e-03
#TE Panel Foam
x0[30] = 9.000e-02
x0[31] = 7.000e-02
x0[32] = 5.000e-02
x0[33] = 3.000e-02
x0[34] = 2.000e-02

Re = 3e6

Np = 0
Tp = 0

funcHist = []
consHist = []

# -------- starting point and bounds --------------
ub = [45, 45, 45, 45, 45, 45, 14, 5.3, 5.3, 5.3, 5.3, 30.0, 30.0, 30.0, 30.0]
lb = [12, 12, 12, 12, 12, 12, 3, 0.4, 0.4, 0.4, 0.4, -10.0, -10.0, -10.0, -10.0]

for i in range(20):
    ub.append(1)
    lb.append(1e-05)

# ub = [45, 45, 45, 45, 45, 45, 14, 5.3, 5.3, 5.3, 5.3]
# lb = [12, 12, 12, 12, 12, 12, 3, 0.4, 0.4, 0.4, 0.4]
# -------------------------------------------------
bounds = []
for i in range(len(lb)):
    bounds.append((lb[i], ub[i]))

rs = ResponseSurface(Re, 12, 45)
cl_spline, cd_spline = rs.extrapolate(False)
#rs.plotting()
foils = [0, 0, 0, 0, 0, 0, 0]
for i in range(len(foils)):
    filename = 'AERO_' + str(i+1) + '.dat'
    foils[i] = filename

def main():
    # initializeAeroStruct()
    initAkima()
    #
    # aeroStruct();
    global rotor

    # aeroInit()
    initializeAeroStruct()
    aeroStruct(x0)



    cons = (#{'type': 'ineq', 'fun': lambda x:  rotor.strainU_spar[[0, 12, 14, 18, 22, 28, 34]]*eta_strain/strain_ult_spar + 1.0},
            #{'type': 'ineq', 'fun': lambda x:  -(rotor.strainL_spar[[0, 12, 14, 18, 22, 28, 34]]*eta_strain/strain_ult_spar) - 1.0},
            #{'type': 'ineq', 'fun': lambda x:  rotor.strainU_te[[0, 8, 12, 14, 18, 22, 28, 34]]*eta_strain/strain_ult_te + 1.0},
            #{'type': 'ineq', 'fun': lambda x:  -(rotor.strainL_te[[0, 8, 12, 14, 18, 22, 28, 34]]*eta_strain/strain_ult_te) - 1.0},
            {'type': 'ineq', 'fun': lambda x:  ((rotor.eps_crit_spar[[10, 12, 14, 20, 23, 27, 31, 33]] - rotor.strainU_spar[[10, 12, 14, 20, 23, 27, 31, 33]]) / strain_ult_spar)},
            {'type': 'ineq', 'fun': lambda x:  ((rotor.eps_crit_te[[10, 12, 13, 14, 21, 28, 33]] - rotor.strainU_te[[10, 12, 13, 14, 21, 28, 33]]) / strain_ult_te)})

    res = opt.minimize(aeroOpt, x0, method='SLSQP', bounds=bounds, jac=True)#, constraints=cons)

    print("Final results are:")
    print(res.x)
    print(res.fun)

    # crd = [3.2612, 4.5709, 3.3178, 1.4621]
    # thta = [13.2783, 7.46036, 2.89317, -0.0878099]
    #
    # opt_prob = Optimization('Aero stuff 4ever', aeroOpt)
    # opt_prob.addVar('x1','c',lower=12,upper=45,value= 40.0)
    # opt_prob.addVar('x2','c',lower=12,upper=45,value= 35.0)
    # opt_prob.addVar('x3','c',lower=12,upper=45,value= 30.0)
    # opt_prob.addVar('x4','c',lower=12,upper=45,value= 25.0)
    # opt_prob.addVar('x5','c',lower=12,upper=45,value= 21.0)
    # opt_prob.addVar('x6','c',lower=12,upper=45,value= 17.0)
    # opt_prob.addVar('tip','c',lower=3,upper=14,value= 7.55)
    # opt_prob.addVar('crd1','c',lower=0.4,upper=5.3,value= crd[0])
    # opt_prob.addVar('crd2','c',lower=0.4,upper=5.3,value= crd[1])
    # opt_prob.addVar('crd3','c',lower=0.4,upper=5.3,value= crd[2])
    # opt_prob.addVar('crd4','c',lower=0.4,upper=5.3,value= crd[3])
    # opt_prob.addVar('thta1','c',lower=-10.0,upper=30.0,value= thta[0])
    # opt_prob.addVar('thta2','c',lower=-10.0,upper=30.0,value= thta[1])
    # opt_prob.addVar('thta3','c',lower=-10.0,upper=30.0,value= thta[2])
    # opt_prob.addVar('thta4','c',lower=-10.0,upper=30.0,value= thta[3])
    # opt_prob.addObj('f')
    # print opt_prob

    # Instantiate Optimizer (PSQP) & Solve Problem
    # alspo = ALPSO()
    # alspo(opt_prob)
    # print opt_prob.solution(0)

    # solve with slsqp
    # slsqp = SLSQP()
    # slsqp.setOption('IPRINT',-1)
    # slsqp(opt_prob)
    # print opt_prob.solution(1)

    print("Final results are:")
    print(res.x)
    print(res.fun)
    cdf_type = 'weibull'
    rotor = rotoraerodefaults.RotorAeroVSVPWithCCBlade(cdf_type)
    plt.figure()
    plt.plot(funcHist)
    plt.title("Function History")
    plt.xlabel("n")
    plt.ylabel("AEP")

    plt.figure()
    plt.plot(consHist)
    plt.title("Constraint Violation History")
    plt.xlabel("n")
    plt.ylabel("Maximum Contraint Violation")
    plt.show()

    print("Function History:")
    print(funcHist)
    print("Constraint Violation History:")
    print(consHist)

def initAkima():
    x = [0.0, 0.18, 0.48, 0.75, 1.0]

    global capTriaxThk
    global capCarbThk
    global tePanelFoamThk
    global tePanelTriaxThk

    capTriaxThk = akima.Akima(x, [3.0e-03, 2.9e-03, 2.8e-03, 2.75e-03, 2.7e-03])
    capCarbThk = akima.Akima(x, [4.2e-02, 2.5e-02, 1.0e-02, 9.0e-03, 6.580e-03])

    tePanelTriaxThk = akima.Akima(x, [3.0e-03, 2.9e-03, 2.8e-03, 2.75e-03, 2.7e-03])
    tePanelFoamThk = akima.Akima(x, [9.000e-02, 7.000e-02, 5.000e-02, 3.000e-02, 2.000e-02])

def aeroOpt(x):
    AEP = obj(x)
    grad = finite_difference(x)

    print 'AEP0 =', AEP/100.0

    global Np
    global Tp
    funcHist.append(-AEP)
    # consHist.append(max(Np - 10000, Tp - 10000))

    fail = 0
    g = []

    return AEP, grad

def obj(x):
    for i in range(6):
        rs.createAeroFile(foils[i], Re, cl_spline, cd_spline, x[i])

    #adjust structure splines
    z = [0.0, 0.18, 0.48, 0.75, 1.0]
    capTriaxThk = akima.Akima(z, x[15:20])
    capCarbThk = akima.Akima(z, x[20:25])
    tePanelTriaxThk = akima.Akima(z, x[25:30])
    tePanelFoamThk = akima.Akima(z, x[30:35])

    AEP, mass = aeroStruct(x)
    retVal = (mass / AEP)*100

    return retVal
    # return -AEP*1e-6

def finite_difference(x0):
    # foils[length] = x0[length]
    step_size = 1e-6

    size = x0.shape
    n = size[0]
    gradient = np.zeros((n, 1))
    for i in range(0, n):
        x_new = deepcopy(x0)
        x_new[i] += step_size
        gradient[i] = (obj(x_new) - obj(x0)).real / step_size
    return gradient

def aeroInit():

    #define geometry
    rotor.r_max_chord = 0.23577  # (Float): location of second control point (generally also max chord)
    # rotor.chord_sub = [3.2612, 4.5709, 3.3178, 1.4621]  # (Array, m): chord at control points
    # rotor.theta_sub = [13.2783, 7.46036, 2.89317, -0.0878099]  # (Array, deg): twist at control points
    rotor.Rhub = 1.5  # (Float, m): hub radius
    rotor.Rtip = 63.0  # (Float, m): tip radius
    rotor.precone = 2.5  # (Float, deg): precone angle
    rotor.tilt = -5.0  # (Float, deg): shaft tilt
    rotor.yaw = 0.0  # (Float, deg): yaw error
    rotor.B = 3  # (Int): number of blades

    #define atmospheric properties and wind speed distribution
    rotor.rho = 1.225  # (Float, kg/m**3): density of air
    rotor.mu = 1.81206e-5  # (Float, kg/m/s): dynamic viscosity of air
    rotor.shearExp = 0.2  # (Float): shear exponent
    rotor.hubHt = 80.0  # (Float, m)
    rotor.cdf_mean_wind_speed = 6.0  # (Float, m/s): mean wind speed of site cumulative distribution function
    rotor.weibull_shape_factor = 2.0  # (Float): shape factor of weibull distribution

    # set relevant control parameters
    rotor.control.Vin = 3.0  # (Float, m/s): cut-in wind speed
    rotor.control.Vout = 25.0  # (Float, m/s): cut-out wind speed
    rotor.control.ratedPower = 5e6  # (Float, W): rated power
    rotor.control.pitch = 0.0  # (Float, deg): pitch angle in region 2 (and region 3 for fixed pitch machines)
    rotor.control.minOmega = 0.0  # (Float, rpm): minimum allowed rotor rotation speed
    rotor.control.maxOmega = 12.0  # (Float, rpm): maximum allowed rotor rotation speed
    # rotor.control.tsr = 7.55  # **dv** (Float): tip-speed ratio in Region 2 (should be optimized externally)

    # set drivetrain type
    rotor.drivetrainType = 'geared'

    #set optional config parameters
    rotor.nSector = 4  # (Int): number of sectors to divide rotor face into in computing thrust and power
    rotor.npts_coarse_power_curve = 20  # (Int): number of points to evaluate aero analysis at
    rotor.npts_spline_power_curve = 200  # (Int): number of points to use in fitting spline to power curve
    rotor.AEP_loss_factor = 1.0  # (Float): availability and other losses (soiling, array, etc.)
    rotor.tiploss = True  # (Bool): include Prandtl tip loss model
    rotor.hubloss = True  # (Bool): include Prandtl hub loss model
    rotor.wakerotation = True  # (Bool): include effect of wake rotation (i.e., tangential induction factor is nonzero)
    rotor.usecd = True  # (Bool): use drag coefficient in computing induction factors

def aeroOnly(x):

    #load airfoils
    basepath = os.path.dirname(os.path.realpath(__file__))

    rotor.chord_sub = [x[7], x[8], x[9], x[10]]  # (Array, m): chord at control points
    rotor.theta_sub = [x[11], x[12], x[13], x[14]]  # (Array, deg): twist at control points
    rotor.control.tsr = x[6]  # **dv** (Float): tip-speed ratio in Region 2 (should be optimized externally)
    # rotor.chord_sub = [x[7], x[8], x[9], x[10]]  # (Array, m): chord at control points
    # rotor.theta_sub = [13.2783, 7.46036, 2.89317, -0.0878099]  # (Array, deg): twist at control points
    # rotor.control.tsr = 7.55  # **dv** (Float): tip-speed ratio in Region 2 (should be optimized externally)

    # load all airfoils
    airfoil_types = [0]*8
    airfoil_types[0] = basepath + os.path.sep + 'Cylinder1.dat'
    airfoil_types[1] = basepath + os.path.sep + 'Cylinder2.dat'
    airfoil_types[2] = basepath + os.path.sep + 'AERO_1.dat'
    airfoil_types[3] = basepath + os.path.sep + 'AERO_2.dat'
    airfoil_types[4] = basepath + os.path.sep + 'AERO_3.dat'
    airfoil_types[5] = basepath + os.path.sep + 'AERO_4.dat'
    airfoil_types[6] = basepath + os.path.sep + 'AERO_5.dat'
    airfoil_types[7] = basepath + os.path.sep + 'AERO_6.dat'

    # place at appropriate radial stations
    af_idx = [0, 0, 1, 2, 3, 3, 4, 5, 5, 6, 6, 7, 7, 7, 7, 7, 7]

    n = len(af_idx)
    af = [0]*n
    for i in range(n):
        af[i] = airfoil_types[af_idx[i]]

    rotor.airfoil_files = af  # (List): paths to AeroDyn-style airfoil files
    rotor.r_af = np.array([0.02222276, 0.06666667, 0.11111057, 0.16666667, 0.23333333, 0.3, 0.36666667,
        0.43333333, 0.5, 0.56666667, 0.63333333, 0.7, 0.76666667, 0.83333333, 0.88888943,
        0.93333333, 0.97777724])    # (Array, m): locations where airfoils are defined on unit radius
    rotor.idx_cylinder = 3  # (Int): index in r_af where cylinder section ends

    # run it and plot the AEP
    rotor.run()

    AEP0 = rotor.AEP

    # paramaters Ryao wants
    Rhub = 1.5
    Rtip = 63.0

    r = np.array([2.8667, 5.6000, 8.3333, 11.7500, 15.8500, 19.9500, 24.0500,
                  28.1500, 32.2500, 36.3500, 40.4500, 44.5500, 48.6500, 52.7500,
                  56.1667, 58.9000, 61.6333])

    # get constraint information
    Uinf = 10.0
    tsr = 7.55
    pitch = 0.0
    Omega = Uinf*tsr/Rtip * 30.0/pi  # convert to RPM
    azimuth = 0.0

    # evaluate distributed loads
    x, y, _, _ = rotor.analysis.ccblade.distributedAeroLoads(Uinf, Omega, pitch, azimuth)

    global Np
    global Tp
    Np = max(x)
    Tp = max(y)

    # print 'AEP0 =', AEP0

    # plt.plot(rotor.V, rotor.P/1e6)
    # plt.xlabel('wind speed (m/s)')
    # plt.ylabel('power (MW)')
    # plt.show()

    # # plot
    # rstar = (r - Rhub) / (Rtip - Rhub)
    #
    # # append zero at root and tip
    # rstar = np.concatenate([[0.0], rstar, [1.0]])
    # Np = np.concatenate([[0.0], Np, [0.0]])
    # Tp = np.concatenate([[0.0], Tp, [0.0]])
    #
    # plt.plot(rstar, Tp/1e3, label='lead-lag')
    # plt.plot(rstar, Np/1e3, label='flapwise')
    # plt.xlabel('blade fraction')
    # plt.ylabel('distributed aerodynamic loads (kN)')
    # plt.legend(loc='upper left')
    # plt.grid()
    # plt.show()

    return AEP0

def aeroStruct(x):
    global capTriaxThk
    global capCarbThk
    global tePanelFoamThk
    global tePanelTriaxThk
    global gBasepath
    global rotor

    basepath = os.path.dirname(os.path.realpath(__file__))

    # load all airfoils
    airfoil_types = [0]*8
    airfoil_types[0] = basepath + os.path.sep + 'Cylinder1.dat'
    airfoil_types[1] = basepath + os.path.sep + 'Cylinder2.dat'
    airfoil_types[2] = basepath + os.path.sep + 'AERO_1.dat'
    airfoil_types[3] = basepath + os.path.sep + 'AERO_2.dat'
    airfoil_types[4] = basepath + os.path.sep + 'AERO_3.dat'
    airfoil_types[5] = basepath + os.path.sep + 'AERO_4.dat'
    airfoil_types[6] = basepath + os.path.sep + 'AERO_5.dat'
    airfoil_types[7] = basepath + os.path.sep + 'AERO_6.dat'

    # place at appropriate radial stations
    af_idx = [0, 0, 1, 2, 3, 3, 4, 5, 5, 6, 6, 7, 7, 7, 7, 7, 7]

    n = len(af_idx)
    af = [0]*n
    for i in range(n):
        af[i] = airfoil_types[af_idx[i]]
    rotor.airfoil_files = af  # (List): names of airfoil file

    # r_max_chord = 0.23577  # (Float): location of second control point (generally also max chord)
    # Rhub = 1.5  # (Float, m): hub radius
    # Rtip = 63.0  # (Float, m): tip radius
    #
    # thick_sub = [x[0], x[1], x[2], x[3], x[4], x[5]]
    #
    # r = np.array([0.02222276, 0.06666667, 0.11111057, 0.2, 0.23333333, 0.3, 0.36666667, 0.43333333,
    #     0.5, 0.56666667, 0.63333333, 0.64, 0.7, 0.83333333, 0.88888943, 0.93333333,
    #     0.97777724])
    #
    # r *= Rtip
    #
    # nth = len(thick_sub)
    # r_cylinder = Rhub + (Rtip-Rhub)
    # # rth = np.linspace(r_cylinder, Rtip, nth)
    # rth = np.linspace(r_max_chord, Rtip, nth-1)
    # rth = np.concatenate([[Rhub], rth])
    # thickness_spline = akima.Akima(rth, thick_sub)
    #
    # thickness, _, _, _ = thickness_spline.interp(r)
    #
    # alpha = np.linspace(-180, 180, 71)
    # Re = 3e6
    # Re1 = np.zeros(1)
    # Re1[0] = Re
    #
    # af = [0]*len(r)
    # for i in range(len(r)):
    #     cl = cl_spline(alpha, thickness[i])
    #     cd = cd_spline(alpha, thickness[i])
    #     af[i] = CCAirfoil(alpha, Re1, cl, cd)
    #
    # rotor.analysis.af = af
    # rotor.aero_extrm.af = af
    # rotor.aero_defl_powercurve.af = af
    # rotor.aero_extrm_forces.af = af
    # rotor.aero_rated.af = af
    # rotor.geom.af = af

    rotor.chord_sub = [x[7], x[8], x[9], x[10]]  # (Array, m): chord at control points
    rotor.theta_sub = [x[11], x[12], x[13], x[14]]  # (Array, deg): twist at control points
    rotor.control.tsr = x[6]  # **dv** (Float): tip-speed ratio in Region 2 (should be optimized externally)

    ncomp = len(rotor.initial_str_grid)
    upper = [0]*ncomp
    lower = [0]*ncomp
    webs = [0]*ncomp
    profile = [0]*ncomp

    web1 = np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 0.4114, 0.4102, 0.4094, 0.3876, 0.3755, 0.3639, 0.345, 0.3342, 0.3313, 0.3274, 0.323, 0.3206, 0.3172, 0.3138, 0.3104, 0.307, 0.3003, 0.2982, 0.2935, 0.2899, 0.2867, 0.2833, 0.2817, 0.2799, 0.2767, 0.2731, 0.2664, 0.2607, 0.2562, 0.1886, -1.0])
    web2 = np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 0.5886, 0.5868, 0.5854, 0.5508, 0.5315, 0.5131, 0.4831, 0.4658, 0.4687, 0.4726, 0.477, 0.4794, 0.4828, 0.4862, 0.4896, 0.493, 0.4997, 0.5018, 0.5065, 0.5101, 0.5133, 0.5167, 0.5183, 0.5201, 0.5233, 0.5269, 0.5336, 0.5393, 0.5438, 0.6114, -1.0])
    web3 = np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0])

    for i in range(ncomp):

        webLoc = []
        if web1[i] != -1:
            webLoc.append(web1[i])
        if web2[i] != -1:
            webLoc.append(web2[i])
        if web3[i] != -1:
            webLoc.append(web3[i])

        if(i < 8 or (i > 12 and i < 36)):
            # set thicknesses for UpperCS spar cap
            rotor.upperCS[i].t[2][1], _, _, _ = capTriaxThk.interp(rotor.initial_str_grid[i]) # cap triax
            rotor.upperCS[i].t[2][3] = rotor.upperCS[i].t[2][1] # cap triax
            rotor.upperCS[i].t[2][2], _, _, _ = capCarbThk.interp(rotor.initial_str_grid[i]) # cap carbon

            # same for lowerCS spar cap
            rotor.lowerCS[i].t[2][1] = rotor.upperCS[i].t[2][1] # cap triax
            rotor.lowerCS[i].t[2][3] = rotor.upperCS[i].t[2][3] # cap triax
            rotor.lowerCS[i].t[2][2] = rotor.upperCS[i].t[2][2] # cap carbon

            #set thicknesses for UpperCS te panel
            rotor.upperCS[i].t[3][1], _, _, _ = tePanelTriaxThk.interp(rotor.initial_str_grid[i]) # triax
            rotor.upperCS[i].t[3][3] = rotor.upperCS[i].t[3][1] # triax
            rotor.upperCS[i].t[3][2], _, _, _ = tePanelFoamThk.interp(rotor.initial_str_grid[i]) # foam

            # same for lowerCS te panel
            rotor.lowerCS[i].t[3][1] = rotor.upperCS[i].t[3][1] # triax
            rotor.lowerCS[i].t[3][3] = rotor.upperCS[i].t[3][3] # triax
            rotor.lowerCS[i].t[3][2] = rotor.upperCS[i].t[3][2] # foam
        elif(i >= 8 and i <= 12):
            # set thicknesses for UpperCS spar cap
            rotor.upperCS[i].t[2][1], _, _, _ = capTriaxThk.interp(rotor.initial_str_grid[i]) # cap triax
            rotor.upperCS[i].t[2][4] = rotor.upperCS[i].t[2][1] # cap triax
            rotor.upperCS[i].t[2][3], _, _, _ = capCarbThk.interp(rotor.initial_str_grid[i]) # cap carbon

            # same for lowerCS spar cap
            rotor.lowerCS[i].t[2][1] = rotor.upperCS[i].t[2][1] # cap triax
            rotor.lowerCS[i].t[2][4] = rotor.upperCS[i].t[2][4] # cap triax
            rotor.lowerCS[i].t[2][3] = rotor.upperCS[i].t[2][3] # cap carbon

            #set thicknesses for UpperCS te panel
            rotor.upperCS[i].t[3][1], _, _, _ = tePanelTriaxThk.interp(rotor.initial_str_grid[i]) # triax
            rotor.upperCS[i].t[3][4] = rotor.upperCS[i].t[3][1] # triax
            rotor.upperCS[i].t[3][3], _, _, _ = tePanelFoamThk.interp(rotor.initial_str_grid[i]) # foam

            # same for lowerCS te panel
            rotor.lowerCS[i].t[3][1] = rotor.upperCS[i].t[3][1] # triax
            rotor.lowerCS[i].t[3][4] = rotor.upperCS[i].t[3][4] # triax
            rotor.lowerCS[i].t[3][3] = rotor.upperCS[i].t[3][3] # foam
        elif(i >= 36):
            # set thicknesses for UpperCS spar cap
            rotor.upperCS[i].t[2][1], _, _, _ = capTriaxThk.interp(rotor.initial_str_grid[i]) # cap triax
            rotor.upperCS[i].t[2][2] = rotor.upperCS[i].t[2][1] # cap triax

            # same for lowerCS spar cap
            rotor.lowerCS[i].t[2][1] = rotor.upperCS[i].t[2][1] # cap triax
            rotor.lowerCS[i].t[2][2] = rotor.upperCS[i].t[2][2] # cap triax

            #set thicknesses for UpperCS te panel
            rotor.upperCS[i].t[3][1], _, _, _ = tePanelTriaxThk.interp(rotor.initial_str_grid[i]) # triax
            rotor.upperCS[i].t[3][2] = rotor.upperCS[i].t[3][1] # triax

            # same for lowerCS te panel
            rotor.lowerCS[i].t[3][1] = rotor.upperCS[i].t[3][1] # triax
            rotor.lowerCS[i].t[3][2] = rotor.upperCS[i].t[3][2] # triax


    # rotor.upperCS = upper  # (List): list of CompositeSection objections defining the properties for upper surface
    # rotor.lowerCS = lower  # (List): list of CompositeSection objections defining the properties for lower surface
    # rotor.websCS = webs  # (List): list of CompositeSection objections defining the properties for shear webs
    # rotor.profile = profile  # (List): airfoil shape at each radial position

    #Run it!!!!
    rotor.run()

    return rotor.AEP, rotor.mass_all_blades

def initializeAeroStruct():
    global rotor
    rotor = RotorSE()

    # set aero grids
    rotor.initial_aero_grid = np.array([0.02222276, 0.06666667, 0.11111057, 0.16666667, 0.23333333, 0.3, 0.36666667,
        0.43333333, 0.5, 0.56666667, 0.63333333, 0.7, 0.76666667, 0.83333333, 0.88888943, 0.93333333,
        0.97777724])  # (Array): initial aerodynamic grid on unit radius
    rotor.initial_str_grid = np.array([0.0, 0.00492790457512, 0.00652942887106, 0.00813095316699, 0.00983257273154,
        0.0114340970275, 0.0130356213234, 0.02222276, 0.024446481932, 0.026048006228, 0.06666667, 0.089508406455,
        0.11111057, 0.146462614229, 0.16666667, 0.195309105255, 0.23333333, 0.276686558545, 0.3, 0.333640766319,
        0.36666667, 0.400404310407, 0.43333333, 0.5, 0.520818918408, 0.56666667, 0.602196371696, 0.63333333,
        0.667358391486, 0.683573824984, 0.7, 0.73242031601, 0.76666667, 0.83333333, 0.88888943, 0.93333333, 0.97777724,
        1.0])  # (Array): initial structural grid on unit radius
    rotor.idx_cylinder_aero = 3  # (Int): first idx in r_aero_unit of non-cylindrical section, constant twist inboard of here
    rotor.idx_cylinder_str = 14  # (Int): first idx in r_str_unit of non-cylindrical section
    rotor.hubFraction = 0.025  # (Float): hub location as fraction of radius

    basepath = os.path.dirname(os.path.realpath(__file__))

    # load all airfoils
    airfoil_types = [0]*8
    airfoil_types[0] = basepath + os.path.sep + 'Cylinder1.dat'
    airfoil_types[1] = basepath + os.path.sep + 'Cylinder2.dat'
    airfoil_types[2] = basepath + os.path.sep + 'AERO_1.dat'
    airfoil_types[3] = basepath + os.path.sep + 'AERO_2.dat'
    airfoil_types[4] = basepath + os.path.sep + 'AERO_3.dat'
    airfoil_types[5] = basepath + os.path.sep + 'AERO_4.dat'
    airfoil_types[6] = basepath + os.path.sep + 'AERO_5.dat'
    airfoil_types[7] = basepath + os.path.sep + 'AERO_6.dat'

    # place at appropriate radial stations
    af_idx = [0, 0, 1, 2, 3, 3, 4, 5, 5, 6, 6, 7, 7, 7, 7, 7, 7]

    n = len(af_idx)
    af = [0]*n
    for i in range(n):
        af[i] = airfoil_types[af_idx[i]]
    rotor.airfoil_files = af  # (List): names of airfoil file

    #define geometric params
    rotor.r_aero = np.array([0.02222276, 0.06666667, 0.11111057, 0.2, 0.23333333, 0.3, 0.36666667, 0.43333333,
        0.5, 0.56666667, 0.63333333, 0.64, 0.7, 0.83333333, 0.88888943, 0.93333333,
        0.97777724])  # (Array): new aerodynamic grid on unit radius
    rotor.r_max_chord = 0.23577  # (Float): location of max chord on unit radius
    rotor.chord_sub = [3.2612, 4.5709, 3.3178, 1.4621]  # (Array, m): chord at control points. defined at hub, then at linearly spaced locations from r_max_chord to tip
    rotor.theta_sub = [13.2783, 7.46036, 2.89317, -0.0878099]  # (Array, deg): twist at control points.  defined at linearly spaced locations from r[idx_cylinder] to tip
    rotor.precurve_sub = [0.0, 0.0, 0.0]  # (Array, m): precurve at control points.  defined at same locations at chord, starting at 2nd control point (root must be zero precurve)
    rotor.delta_precurve_sub = [0.0, 0.0, 0.0]  # (Array, m): adjustment to precurve to account for curvature from loading
    rotor.sparT = [0.05, 0.047754, 0.045376, 0.031085, 0.0061398]  # (Array, m): spar cap thickness parameters
    rotor.teT = [0.1, 0.09569, 0.06569, 0.02569, 0.00569]  # (Array, m): trailing-edge thickness parameters
    rotor.bladeLength = 61.5  # (Float, m): blade length (if not precurved or swept) otherwise length of blade before curvature
    rotor.delta_bladeLength = 0.0  # (Float, m): adjustment to blade length to account for curvature from loading
    rotor.precone = 2.5  # (Float, deg): precone angle
    rotor.tilt = 5.0  # (Float, deg): shaft tilt
    rotor.yaw = 0.0  # (Float, deg): yaw error
    rotor.nBlades = 3  # (Int): number of blades

    # load airfoil data
    basepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), '5MW_AFFiles')

    #atmospheric data
    rotor.rho = 1.225  # (Float, kg/m**3): density of air
    rotor.mu = 1.81206e-5  # (Float, kg/m/s): dynamic viscosity of air
    rotor.shearExp = 0.2  # (Float): shear exponent
    rotor.hubHt = 90.0  # (Float, m): hub height
    rotor.turbine_class = 'I'  # (Enum): IEC turbine class
    rotor.turbulence_class = 'B'  # (Enum): IEC turbulence class class
    rotor.cdf_reference_height_wind_speed = 90.0  # (Float): reference hub height for IEC wind speed (used in CDF calculation)
    rotor.g = 9.81  # (Float, m/s**2): acceleration of gravity

    # steady state control conditions
    rotor.control.Vin = 3.0  # (Float, m/s): cut-in wind speed
    rotor.control.Vout = 25.0  # (Float, m/s): cut-out wind speed
    rotor.control.ratedPower = 5e6  # (Float, W): rated power
    rotor.control.minOmega = 0.0  # (Float, rpm): minimum allowed rotor rotation speed
    rotor.control.maxOmega = 12.0  # (Float, rpm): maximum allowed rotor rotation speed
    rotor.control.tsr = 7.55  # (Float): tip-speed ratio in Region 2 (should be optimized externally)
    rotor.control.pitch = 0.0  # (Float, deg): pitch angle in region 2 (and region 3 for fixed pitch machines)
    rotor.pitch_extreme = 0.0  # (Float, deg): worst-case pitch at survival wind condition
    rotor.azimuth_extreme = 0.0  # (Float, deg): worst-case azimuth at survival wind condition
    rotor.VfactorPC = 0.7  # (Float): fraction of rated speed at which the deflection is assumed to representative throughout the power curve calculation

    # optional parameters
    rotor.nSector = 4  # (Int): number of sectors to divide rotor face into in computing thrust and power
    rotor.npts_coarse_power_curve = 20  # (Int): number of points to evaluate aero analysis at
    rotor.npts_spline_power_curve = 200  # (Int): number of points to use in fitting spline to power curve
    rotor.AEP_loss_factor = 1.0  # (Float): availability and other losses (soiling, array, etc.)
    rotor.drivetrainType = 'geared'  # (Enum)
    rotor.nF = 5  # (Int): number of natural frequencies to compute
    rotor.dynamic_amplication_tip_deflection = 1.35  # (Float): a dynamic amplification factor to adjust the static deflection calculation

    # material and composite layup data
    basepath = os.path.join(os.path.dirname(os.path.realpath(__file__)), '5MW_PreCompFiles')

    materials = Orthotropic2DMaterial.listFromPreCompFile(os.path.join(basepath, 'materials.inp'))

    ncomp = len(rotor.initial_str_grid)
    upper = [0]*ncomp
    lower = [0]*ncomp
    webs = [0]*ncomp
    profile = [0]*ncomp

    rotor.leLoc = np.array([0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.498, 0.497, 0.465, 0.447, 0.43, 0.411,
        0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4,
        0.4, 0.4, 0.4, 0.4])    # (Array): array of leading-edge positions from a reference blade axis (usually blade pitch axis). locations are normalized by the local chord length. e.g. leLoc[i] = 0.2 means leading edge is 0.2*chord[i] from reference axis.  positive in -x direction for airfoil-aligned coordinate system
    rotor.sector_idx_strain_spar = [2]*ncomp  # (Array): index of sector for spar (PreComp definition of sector)
    rotor.sector_idx_strain_te = [3]*ncomp  # (Array): index of sector for trailing-edge (PreComp definition of sector)
    web1 = np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 0.4114, 0.4102, 0.4094, 0.3876, 0.3755, 0.3639, 0.345, 0.3342, 0.3313, 0.3274, 0.323, 0.3206, 0.3172, 0.3138, 0.3104, 0.307, 0.3003, 0.2982, 0.2935, 0.2899, 0.2867, 0.2833, 0.2817, 0.2799, 0.2767, 0.2731, 0.2664, 0.2607, 0.2562, 0.1886, -1.0])
    web2 = np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 0.5886, 0.5868, 0.5854, 0.5508, 0.5315, 0.5131, 0.4831, 0.4658, 0.4687, 0.4726, 0.477, 0.4794, 0.4828, 0.4862, 0.4896, 0.493, 0.4997, 0.5018, 0.5065, 0.5101, 0.5133, 0.5167, 0.5183, 0.5201, 0.5233, 0.5269, 0.5336, 0.5393, 0.5438, 0.6114, -1.0])
    web3 = np.array([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0])
    rotor.chord_str_ref = np.array([3.2612, 3.3100915356, 3.32587052924, 3.34159388653, 3.35823798667, 3.37384375335,
        3.38939112914, 3.4774055542, 3.49839685, 3.51343645709, 3.87017220335, 4.04645623801, 4.19408216643,
         4.47641008477, 4.55844487985, 4.57383098262, 4.57285771934, 4.51914315648, 4.47677655262, 4.40075650022,
         4.31069949379, 4.20483735936, 4.08985563932, 3.82931757126, 3.74220276467, 3.54415796922, 3.38732428502,
         3.24931446473, 3.23421422609, 3.22701537997, 3.21972125648, 3.08979310611, 2.95152261813, 2.330753331,
         2.05553464181, 1.82577817774, 1.5860853279, 1.4621])  # (Array, m): chord distribution for reference section, thickness of structural layup scaled with reference thickness (fixed t/c for this case)

    for i in range(ncomp):

        webLoc = []
        if web1[i] != -1:
            webLoc.append(web1[i])
        if web2[i] != -1:
            webLoc.append(web2[i])
        if web3[i] != -1:
            webLoc.append(web3[i])

        upper[i], lower[i], webs[i] = CompositeSection.initFromPreCompLayupFile(os.path.join(basepath, 'layup_' + str(i+1) + '.inp'), webLoc, materials)
        profile[i] = Profile.initFromPreCompFile(os.path.join(basepath, 'shape_' + str(i+1) + '.inp'))

    rotor.materials = materials  # (List): list of all Orthotropic2DMaterial objects used in defining the geometry
    rotor.upperCS = upper  # (List): list of CompositeSection objections defining the properties for upper surface
    rotor.lowerCS = lower  # (List): list of CompositeSection objections defining the properties for lower surface
    rotor.websCS = webs  # (List): list of CompositeSection objections defining the properties for shear webs
    rotor.profile = profile  # (List): airfoil shape at each radial position

    # fatigue analysis
    rotor.rstar_damage = np.array([0.000, 0.022, 0.067, 0.111, 0.167, 0.233, 0.300, 0.367, 0.433, 0.500,
        0.567, 0.633, 0.700, 0.767, 0.833, 0.889, 0.933, 0.978])  # (Array): nondimensional radial locations of damage equivalent moments
    rotor.Mxb_damage = 1e3*np.array([2.3743E+003, 2.0834E+003, 1.8108E+003, 1.5705E+003, 1.3104E+003,
        1.0488E+003, 8.2367E+002, 6.3407E+002, 4.7727E+002, 3.4804E+002, 2.4458E+002, 1.6339E+002,
        1.0252E+002, 5.7842E+001, 2.7349E+001, 1.1262E+001, 3.8549E+000, 4.4738E-001])  # (Array, N*m): damage equivalent moments about blade c.s. x-direction
    rotor.Myb_damage = 1e3*np.array([2.7732E+003, 2.8155E+003, 2.6004E+003, 2.3933E+003, 2.1371E+003,
        1.8459E+003, 1.5582E+003, 1.2896E+003, 1.0427E+003, 8.2015E+002, 6.2449E+002, 4.5229E+002,
        3.0658E+002, 1.8746E+002, 9.6475E+001, 4.2677E+001, 1.5409E+001, 1.8426E+000])  # (Array, N*m): damage equivalent moments about blade c.s. y-direction
    rotor.strain_ult_spar = 1.0e-2  # (Float): ultimate strain in spar cap
    rotor.strain_ult_te = 2500*1e-6 * 2   # (Float): uptimate strain in trailing-edge panels, note that I am putting a factor of two for the damage part only.
    rotor.eta_damage = 1.35*1.3*1.0  # (Float): safety factor for fatigue
    rotor.m_damage = 10.0  # (Float): slope of S-N curve for fatigue analysis
    rotor.N_damage = 365*24*3600*20.0  # (Float): number of cycles used in fatigue analysis  TODO: make function of rotation speed

    # Run it!!!!
    # rotor.run()
    #
    # print 'AEP =', rotor.AEP
    # print 'diameter =', rotor.diameter
    # print 'ratedConditions.V =', rotor.ratedConditions.V
    # print 'ratedConditions.Omega =', rotor.ratedConditions.Omega
    # print 'ratedConditions.pitch =', rotor.ratedConditions.pitch
    # print 'ratedConditions.T =', rotor.ratedConditions.T
    # print 'ratedConditions.Q =', rotor.ratedConditions.Q
    # print 'mass_one_blade =', rotor.mass_one_blade
    # print 'mass_all_blades =', rotor.mass_all_blades
    # print 'I_all_blades =', rotor.I_all_blades
    # print 'freq =', rotor.freq
    # print 'tip_deflection =', rotor.tip_deflection
    # print 'root_bending_moment =', rotor.root_bending_moment
    #
    # plt.figure()
    # plt.plot(rotor.V, rotor.P/1e6)
    # plt.xlabel('wind speed (m/s)')
    # plt.xlabel('power (W)')
    #
    # plt.figure()
    # plt.plot(rotor.spline.r_str, rotor.strainU_spar, label='suction')
    # plt.plot(rotor.spline.r_str, rotor.strainL_spar, label='pressure')
    # plt.plot(rotor.spline.r_str, rotor.eps_crit_spar, label='critical')
    # plt.ylim([-5e-3, 5e-3])
    # plt.xlabel('r')
    # plt.ylabel('strain')
    # plt.legend()
    #
    # plt.figure()
    # plt.plot(rotor.spline.r_str, rotor.strainU_te, label='suction')
    # plt.plot(rotor.spline.r_str, rotor.strainL_te, label='pressure')
    # plt.plot(rotor.spline.r_str, rotor.eps_crit_te, label='critical')
    # plt.ylim([-5e-3, 5e-3])
    # plt.xlabel('r')
    # plt.ylabel('strain')
    # plt.legend()
    #
    # plt.show()


main()